<?php

namespace App\Providers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $extra = [
            'trackId' => date("YmdHis").'_'.Str::random(),
            'ip'      => request()->ip(),
            'url'     => request()->url()
        ];
        $this->app->make('log')->pushProcessor(function ($record) use ($extra) {
            $record['extra'] = $extra;
            return $record;
        });

        Log::channel('sql')->pushProcessor(function ($record) use ($extra) {
            $record['extra'] = $extra;
            return $record;
        });
    }
}
