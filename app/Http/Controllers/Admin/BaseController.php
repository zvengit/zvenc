<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    use ApiResponse;

    protected $pageSize = 20;
    protected $pageName = 'page_no';

    public function __construct()
    {
        //$this->middleware('auth:api');
        //var_dump(request()->all());
        $this->pageSize = request('page_size') ? request('page_size') : $this->pageSize;
    }
}
