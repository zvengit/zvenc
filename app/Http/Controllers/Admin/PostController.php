<?php

namespace App\Http\Controllers\Admin;

use App\Models\Post;
use App\Models\PostTag;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class PostController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $query = Post::query();

        if(!empty($input['title'])){
            $query->where('title','like',"%{$input['title']}%");
        }

        $paginator = $query->with('tags')->paginate($this->pageSize, ['*'], $this->pageName);
        return $this->success($this->paginatorData($paginator));
    }

    public function create(Request $request)
    {
        $rules = [
            'title'     => ['required'],
            'author'    => ['present'],
            'tag_ids'   => ['array'],
            'tag_ids.*' => [Rule::exists('tags','id')],
            'views'     => ['integer'],
            'content'   => ['string'],
            'status'    => [Rule::in(array_keys(Post::getStatusMap()))]
        ];
        $this->validate($request, $rules);

        $tagIds = $request->input('tag_ids');

        $data = [];
        $data['title']      = $request->input('title');
        $data['author']     = $request->input('author');
        $data['views']      = $request->input('views',0);
        $data['content']    = $request->input('content');
        $data['status']     = $request->input('status', Post::STATUS_ON);

        $id = Post::query()->insertGetId($data);
        if($id && $tagIds){
            foreach($tagIds as $tagId){
                $relData = ['post_id' => $id, 'tag_id' => $tagId];
                PostTag::query()->create($relData);
            }
        }

        return $this->success();
    }

    public function detail(Request $request)
    {
        $id = $request->input('id');
        $result = Post::query()->findOrFail($id);
        $result['tag_ids'] = PostTag::query()->where('post_id',$id)->pluck('tag_id')->toArray();
        return $this->success($result);
    }

    public function update(Request $request)
    {
        $rules = [
            'id'        => ['required'],
            'title'     => ['required'],
            'author'    => ['present'],
            'tag_ids'   => ['array'],
            'tag_ids.*' => [Rule::exists('tags','id')],
            'views'     => ['integer'],
            'content'   => ['string'],
            'status'    => [Rule::in(array_keys(Post::getStatusMap()))]
        ];
        $this->validate($request, $rules);

        $id     = $request->input('id');
        $tagIds = $request->input('tag_ids');

        $data = [];
        $data['title']      = $request->input('title');
        $data['author']     = $request->input('author');
        $data['views']      = $request->input('views',0);
        $data['content']    = $request->input('content');
        $data['status']     = $request->input('status', Post::STATUS_ON);

        Post::query()->where('id', $id)->update($data);

        // 已有标签
        $bindTagIds = PostTag::query()->where('post_id', $id)->pluck('tag_id')->toArray();
        foreach($bindTagIds as $tagId){
            if(!in_array($tagId, $tagIds)){
                PostTag::query()->where('post_id', $id)->where('tag_id', $tagId)->delete();
            }
        }

        // 新绑标签
        foreach($tagIds as $tagId){
            if(!in_array($tagId, $bindTagIds)){
                $relData = ['post_id' => $id, 'tag_id' => $tagId];
                PostTag::query()->create($relData);
            }
        }

        return $this->success();
    }

    public function delete(Request $request)
    {
        $id = $request->input('id',null);
        Post::query()->where('id',$id)->delete();
        return $this->success();
    }
}
