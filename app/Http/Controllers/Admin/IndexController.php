<?php

namespace App\Http\Controllers\Admin;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class IndexController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        Config::set('view.paths', array(resource_path('assets/views')));
    }

    public function index(Request $request)
    {
        $a['a'] = 111;
       return view('index')->with($a);
    }
}
