<?php

namespace App\Http\Controllers\Admin;

use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class TagController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $tagName = $request->input('tag_name');

        $where = [];
        $tagName && $where[] = ['tag_name', 'like', "%{$tagName}%"];

        $list = Tag::query()->where($where)->get()->toArray();
        return $this->success($list);
    }

    public function create(Request $request)
    {
        $rules = [
            'tag_name' => ['required', 'unique:tags,tag_name']
        ];
        $this->validate($request, $rules);


        Tag::query()->create($request->all());
        return $this->success();
    }

    public function update(Request $request)
    {
        $id = $request->input('id',null);
        $request->validate([
            'id'        => 'required|integer',
            'tag_name'  => 'required|unique:tags,tag_name,'.$id,
        ]);

        $data = $request->only(['tag_name']);
        Tag::query()->where('id',$id)->update($data);
        return $this->success();
    }

    public function delete(Request $request)
    {
        $id = $request->input('id',null);
        Tag::query()->where('id',$id)->delete();
        return $this->success();
    }
}
