<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        if(request('password')){
            request()->offsetSet('password',Hash::make(request('password')));
        }
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $query = User::query();

        if(!empty($input['username'])){
            $query->where('username','like',"%{$input['username']}%");
        }
        if(!empty($input['email'])){
            $query->where('email','like',"%{$input['email']}%");
        }

        $paginator = $query->paginate($this->pageSize, ['*'], $this->pageName);
        return $this->success($this->paginatorData($paginator));
    }

    public function create(Request $request)
    {
        $request->validate([
            'username' => 'required|unique:users',
            'email'    => 'email|unique:users',
            'password' => 'required'
        ]);

        User::query()->create($request->all());
        return $this->success();
    }

    public function update(Request $request)
    {
        $id = $request->input('id',null);
        $request->validate([
            'id'        => 'required|integer',
            'username'  => 'required|unique:users,username,'.$id,
            'email'     => 'nullable|email|unique:users,email,'.$id,
        ]);

        $data = $request->only(['username','email','password']);
        User::query()->where('id',$id)->update($data);
        return $this->success();
    }
}
