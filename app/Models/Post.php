<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title','author','content','status','views'
    ];

    const STATUS_ON  = 1;
    const STATUS_OFF = 0;


    public function tags()
    {
        return $this->belongsToMany(Tag::class)->select('tag_id as id','tag_name');
    }

    public static function getStatusMap()
    {
        return [
            self::STATUS_OFF => '否',
            self::STATUS_ON  => '是'
        ];
    }
}
