<?php

namespace App\Traits;


use App\Enums\CodeEnum;

Trait ApiResponse
{
    public static  function paginatorData($paginator)
    {
        $paginator = is_array($paginator) ?: $paginator->toArray();
        $data = [
            'total'         => $paginator['total'],
            'page_no'       => $paginator['current_page'],
            'page_size'     => $paginator['per_page'],
            'list'          => $paginator['data']
        ];
        return $data;
    }

    public function success($data = [],$codeEnum = CodeEnum::SUCCESS)
    {
        $back = [
            'code' => $codeEnum[0],
            'msg'  => $codeEnum[1],
            'data' => $data
        ];
        return response()->json($back);
    }

    public function error($codeEnum = CodeEnum::ERROR_UNKNOW,$data = [])
    {
        $back = [
            'code' => $codeEnum[0],
            'msg'  => $codeEnum[1],
            'data' => $data
        ];
        return response()->json($back);
    }
}
