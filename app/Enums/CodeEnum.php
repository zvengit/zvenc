<?php

namespace App\Enums;


class CodeEnum extends Enum
{
    const SUCCESS               = [0,'success'];

    // 公共错误
    const ERROR_UNKNOW          = [1000, '未知错误'];
    const ERROR_SERVER          = [1001, '服务器错误'];
    const ERROR_MISS_PARAM      = [1002, '缺少参数'];
    const ERROR_NO_RECORD       = [1003, '记录不存在'];
    const ERROR_PARAMS          = [1004, '参数校验错误'];
    const ERROR_NOT_FOUND       = [1005, '资源不存在'];
    const ERROR_EXISTS          = [1006, '记录已存在'];
    const ERROR_LOGIN           = [1007, '请先登录'];
    const ERROR_LOGIN_PARAM     = [1008, '缺少账号或密码'];
    const ERROR_ACCOUNT         = [1009, '账号或密码错误'];


    // 业务错误
    const ERROR_USERNAME_EXIST  = [2002, '用户名已存在'];
    const ERROR_EMAIL_EXIST     = [2003, '邮箱已存在'];
}
