<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class ApiException extends Exception
{

    public function __construct(array $codeEnum,$message = "",Throwable $previous = null)
    {
        $message = $message ?: $codeEnum[1];
        parent::__construct($message,$codeEnum[0],$previous);
    }
}
