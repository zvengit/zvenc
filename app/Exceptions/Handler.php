<?php

namespace App\Exceptions;

use App\Enums\CodeEnum;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Traits\ApiResponse;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    use ApiResponse;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if($e instanceof ApiException){
            $err = [$e->getCode(),$e->getMessage()];
            return $this->error($err);
        }
        if($e instanceof ModelNotFoundException){
            return $this->error(CodeEnum::ERROR_NOT_FOUND);
        }
//
//        $error = $this->convertValidationExceptionToResponse($e, $request);
//        var_dump($error);die();
//
        if($e instanceof ValidationException){
           // return $this->convertValidationExceptionToResponse($e, $request);
            $errors = [];
            foreach($e->errors() as $field=>$errs){
                $errors[$field] = $errs[0];
            }
            return $this->error(CodeEnum::ERROR_PARAMS,$errors);
        }
        return parent::render($request, $e);
    }
}
