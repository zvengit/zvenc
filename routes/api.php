<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::namespace('Auth')->group(function(){
    Route::any('auth/login', 'AuthController@login')->name('auth.login');
    Route::any('auth/logout', 'AuthController@logout')->name('auth.logout');
    Route::any('auth/refresh', 'AuthController@refresh')->name('auth.refresh');
    Route::any('auth/me', 'AuthController@me')->name('auth.me');
});

Route::namespace('Auth')->group(function(){
   // Route::any('register', 'RegisterController@register')->name('register');
    Route::any('register', 'AuthController@store')->name('register');
});

Route::namespace('Admin')->group(function(){
    Route::any('user/index', 'UserController@index')->name('user.index');
    Route::any('user/create', 'UserController@create')->name('user.create');
    Route::any('user/update', 'UserController@update')->name('user.update');

    Route::any('tag/index', 'TagController@index')->name('tag.index');
    Route::any('tag/create', 'TagController@create')->name('tag.create');
    Route::any('tag/update', 'TagController@update')->name('tag.update');
    Route::any('tag/delete', 'TagController@delete')->name('tag.delete');

    Route::any('post/index', 'PostController@index')->name('post.index');
    Route::any('post/create', 'PostController@create')->name('post.create');
    Route::any('post/update', 'PostController@update')->name('post.update');
    Route::any('post/detail', 'PostController@detail')->name('post.detail');
    Route::any('post/delete', 'PostController@delete')->name('post.delete');

    Route::post('file/upload', 'FileController@upload')->name('file.upload');



    Route::any('index','IndexController@index');
});
