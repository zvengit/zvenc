/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : zvenc

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 10/11/2020 20:19:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for post_tag
-- ----------------------------
DROP TABLE IF EXISTS `post_tag`;
CREATE TABLE `post_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL DEFAULT '0',
  `tag_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk` (`post_id`,`tag_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of post_tag
-- ----------------------------
BEGIN;
INSERT INTO `post_tag` VALUES (1, 1, 2, '2020-11-08 15:09:27', '2020-11-08 15:09:32');
INSERT INTO `post_tag` VALUES (2, 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
COMMIT;

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL DEFAULT '',
  `author` varchar(20) NOT NULL DEFAULT '',
  `content` text,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `views` int(11) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `title` (`title`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of posts
-- ----------------------------
BEGIN;
INSERT INTO `posts` VALUES (1, 'zhangdesanswws', '', NULL, 1, 0, '2020-11-08 15:08:31', '2020-11-08 15:08:31');
INSERT INTO `posts` VALUES (2, 'dedezhangdesanswws', 'admin', '我的文章AC', 1, 0, '2020-11-08 15:08:32', '2020-11-08 15:08:32');
INSERT INTO `posts` VALUES (3, 'zhangdesanswws', 'admin', '我的文章AC', 1, 0, '2020-11-08 15:08:33', '2020-11-08 15:08:33');
INSERT INTO `posts` VALUES (4, 'zhangdesanswws', 'admin', '我的文章AC', 1, 0, '2020-11-08 15:08:35', '2020-11-08 15:08:35');
INSERT INTO `posts` VALUES (5, 'zhangdesanswws', 'admin', '我的文章AC', 1, 0, '2020-11-08 15:08:37', '2020-11-08 15:08:37');
COMMIT;

-- ----------------------------
-- Table structure for tags
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(20) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tagname` (`tag_name`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tags
-- ----------------------------
BEGIN;
INSERT INTO `tags` VALUES (1, 'php', '2020-11-08 15:08:23', '2020-11-08 15:08:23');
INSERT INTO `tags` VALUES (2, 'linux', '2020-11-08 15:08:25', '2020-11-08 15:08:25');
INSERT INTO `tags` VALUES (3, 'sql', '2020-11-08 15:08:26', '2020-11-08 15:08:26');
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL DEFAULT '',
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 'admin', 'd@qq.com', '$2y$10$zMGM2c8q4F/sWZYviLtk4Ov6oWGE7XHdJOR.0Cse2lXSzCN7yHclK', '2020-11-08 14:45:54', '2020-11-08 14:45:54');
INSERT INTO `users` VALUES (8, 'zhangssanss', 'qs@qq.com', '$2y$10$Zyar1FEqcl6i2CF9LZLT/uY6441pPT8q2/V3RWX8g2BKH2l50N.Vi', '2020-11-08 13:37:03', '2020-11-08 13:37:03');
INSERT INTO `users` VALUES (9, 'zhangsansjs', 'admin@qq.com', '$2y$10$f6BgB59/fgiOUoS604l/Ded0SMnsHtvNXJVYxo2zxyGePHkZBjc/K', '2020-11-08 02:56:58', '2020-11-08 02:56:58');
INSERT INTO `users` VALUES (10, 'zhangsanss', 'q@qq.com', '$2y$10$VOLchJ6Q3kkElpwlTfp0COMOifpHW4RdLlZwGc9Ik3aE4wpB6Bjna', '2020-11-08 11:42:40', '2020-11-08 11:42:40');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
