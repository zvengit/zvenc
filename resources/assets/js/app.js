window.Vue = require('vue');
window._ = require('lodash');

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);
// Vue.prototype.$ELEMENT = { size: 'small'};

import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
Vue.use(mavonEditor)

import Cookies from 'js-cookie'
import router from './router.js';

import Layout from './components/layout'


const app = new Vue({
        el: '#app',
        router,
        render: h => h(Layout),
    })
;
