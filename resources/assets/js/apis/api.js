import http from '../utils/http'
import request from '../utils/request'
//
/**
 *  @parms resquest 请求地址 例如：http://197.82.15.15:8088/request/...
 *  @param '/testIp'代表vue-cil中config，index.js中配置的代理
 */
let resquest = "/testIp/request/"

// get请求
export default{
    getPostList(params){
        return http.post('/api/post/index', params)
    },
    getPostDetail(params){
        return http.post('/api/post/detail', params)
    },
    postCreate(params){
        return http.post('/api/post/create', params)
    },
    postUpdate(params){
        return http.post('/api/post/update', params)
    },
    getTagList(params){
        return http.post('/api/tag/index', params)
    },
    tagDelete(params){
        return http.post('/api/tag/delete', params)
    },
    tagCreate(params){
        return http.post('/api/tag/create',params)
    },
    // postFormAPI(params){
    //     return http.post(`${resquest}/postForm.json`,params)
    // }
     uploadFile(data) {
        return request({
            url: '/api/file/upload',
            method: 'post',
            data,
            headers: { 'Content-Type': 'multipart/form-data' }
        })
    }
}
