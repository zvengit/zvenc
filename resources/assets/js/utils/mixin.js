const mixin = {
    data: function () {
        return {
            pageSizeArr:[2,10, 20, 30, 50],
            pageSize: 10,
            pageNo:1,
            total: 0,
            layout:'total, sizes, prev, pager, next, jumper'
        }
    },
    methods:{
        handleSizeChange(val) {
            this.pageNo = 1
            this.pageSize = val
            this.getList()
        },
        handleCurrentChange(val) {
            this.pageNo = val
            this.getList()
        }
    }
}

export default mixin;
