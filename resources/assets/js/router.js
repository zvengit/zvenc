import Vue from "vue";
import VueRouter from "vue-router";

import index from './components/index'
import post from './components/post'
import postForm from './components/post-form'
import detail from './components/detail'
import err404 from './components/404'
import tag from './components/tag'

//全局方法 Vue.use() 使用插件，需要在调用 new Vue() 启动应用之前完成：
Vue.use(VueRouter);


export default new VueRouter({
    saveScrollPosition: true,
    routes: [
        {
            path: '/',
            redirect: '/index'
        },
        {
            name: 'index',
            path: '/index',
            component: resolve => void (require(['./components/index.vue'], resolve)),
        },
        {
            name: 'post',
            path: '/post',
            component: post,
        },
        {
            name: 'detail',
            path: '/detail',
            component: postForm,
        },
        {
            name: 'tag',
            path: '/tag',
            component: tag,
        },
        {
            name: '404',
            path: '*',
            component: err404,
        }
    ]
})
