<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <title>zvenc admin</title>
    </head>
    <body>
        <div id="app">
        </div>
        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>
