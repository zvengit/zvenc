const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .webpackConfig({
        resolve: {
            alias: {
                '@': path.resolve(__dirname,'resources/assets/js')
            }
        },
    });

//网上找的改动方案
//
// let mix = require('laravel-mix');
//
// //const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
//
// mix.js('resources/assets/js/home.js', 'public/backend/js').version().webpackConfig({
//     plugins: [
//         //new BundleAnalyzerPlugin()
//     ],
//     output: {
//         chunkFilename: 'backend/js/chunk/[name].js?v=[chunkHash]'
//     },
//     resolve: {
//         alias: {
//             Helper: path.resolve(__dirname, 'resources/assets/js/helper'),
//             Css: path.resolve(__dirname, 'resources/assets/css'),
//             Sass: path.resolve(__dirname, 'resources/assets/sass'),
//             components: path.resolve(__dirname, 'resources/assets/js/views/home/components')
//         }
//     },
//     externals: {
//         'element-ui': 'ELEMENT',
//         'axios': 'axios',
//         'vue': 'Vue',
//         'vue-router': 'VueRouter',
//         'lodash': '_'
//     }
// });
